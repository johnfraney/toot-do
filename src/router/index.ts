import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '@/views/Home.vue';
import ToDoList from '@/views/ToDoList.vue';
import Calendar from '@/views/Calendar.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/lists/:id',
    name: 'ToDoList',
    component: ToDoList,
  },
  {
    path: '/calendar',
    name: 'Calendar',
    component: Calendar,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
