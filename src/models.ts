import { v4 as uuidv4 } from 'uuid';

class Model {
  id: string

  created: Date

  constructor() {
    this.id = uuidv4();
    this.created = new Date();
  }
}

export class ToDoItem extends Model {
  title: string

  description = ''

  complete = false

  due?: Date

  listId: string

  constructor(title: string, listId: string) {
    super();
    this.title = title;
    this.listId = listId;
  }
}

export class ToDoList extends Model {
  title: string

  constructor(title: string) {
    super();
    this.title = title;
  }
}
