import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';
import { ToDoItem, ToDoList } from '@/models';

Vue.use(Vuex);

interface ToDoItemMap {
  [id: string]: ToDoItem;
}

interface ToDoListMap {
  [id: string]: ToDoList;
}

const vuexPersist = new VuexPersist({
  key: 'toot-doot',
  storage: window.localStorage,
});

export default new Vuex.Store({
  state: {
    lists: {},
    items: {},
  },

  getters: {
    itemList: (state) => Object.values(state.items),
  },

  mutations: {
    updateItemComplete(state, item: ToDoItem) {
      Vue.set((state.items as ToDoItemMap)[item.id], 'complete', item.complete);
    },

    addList(state, list: ToDoList) {
      Vue.set(state.lists, list.id, list);
    },

    updateListTitle(state, list: ToDoList) {
      Vue.set((state.lists as ToDoListMap)[list.id], 'title', list.title);
    },

    deleteList(state, list: ToDoList) {
      const items: ToDoItem[] = Object.values(state.items);
      items.filter((filterItem) => filterItem.listId === list.id).forEach((item) => {
        Vue.delete(state.items, item.id);
      });
      Vue.delete(state.lists, list.id);
    },

    addItem(state, item: ToDoItem) {
      Vue.set(state.items, item.id, item);
    },

    updateItem(state, item: ToDoItem) {
      Vue.set(state.items, item.id, item);
    },

    deleteItem(state, item: ToDoItem) {
      Vue.delete(state.items, item.id);
    },
  },

  plugins: [vuexPersist.plugin],
});
